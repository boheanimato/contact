create database contact_db;
use contact_db;

CREATE TABLE Contact(
  ContactId         INT(10)  NOT NULL PRIMARY KEY AUTO_INCREMENT,
  LastUpdate        DATETIME NOT NULL,
  Name              VARCHAR(20) NOT NULL,
  Kana              VARCHAR(60)  NOT NULL,
  Zip              	CHAR(7)   DEFAULT '',
  Address           VARCHAR(200) DEFAULT '',
  Tel               VARCHAR(20)  DEFAULT '' NOT NULL,
  EMail             VARCHAR(100) DEFAULT '' NOT NULL,
  BirthYear         SMALLINT,
  BirthMonth        TINYINT,
  BirthDay          TINYINT,
  Sex               TINYINT NOT NULL,
  Contact           VARCHAR(500) DEFAULT '' NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET utf8;