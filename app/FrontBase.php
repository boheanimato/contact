<?php
/**
 * FrontBase フロントエンド共通ベースクラス
 *
 * @author     ashibuya
 */
class FrontBase
{
    //共通パラメータ
    protected $strAction ='';
    protected $strCategory='';

    //インスタンス
    public $objReq     = null;
    public $objView    = null;

    /**
     * __construct
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        //--------------------------------
        // Create Instance
        //--------------------------------
        $this->objReq = new WebRequest();
        $this->objView = new ViewManager();
        $this->objView->setDirectory();

    }
    //------------------------------------------------------------------------------
    // Utility
    //------------------------------------------------------------------------------

    /**
     * showNotFoundPage
     *
     * @access public
     * @param string $template
     * @return void
     */
    public function showNotFoundPage($template)
    {
        header('HTTP/1.1 404 Not Found');
        $this->objView->setTmplateName($template);
        $this->objView->nativeShow();
        exit();
    }



}
?>