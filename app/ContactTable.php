<?php
/**
 * ContactTable
 *
 * このクラスではDB操作をします
 *
 *
 * @author ashibuya

 */
class ContactTable {

    private $objDb = null;
    private $aryParam = array();

    private $aryContactCol = array(
        'LastUpdate'
        ,'Name'
        ,'Kana'
        ,'Zip'
        ,'Address'
        ,'Tel'
        ,'EMail'
        ,'BirthYear'
        ,'BirthMonth'
        ,'BirthDay'
        ,'Sex'
        ,'Contact'
    );

    /**
     * __construct
     * @access public
     * @return void
     */
    public function __construct($aryReq)
    {

        $this->objDb = new DatabaseManager();
        $this->aryParam = $aryReq;
    }

    /**
     * exec_insert
     * @access public
     * @return void
     */
    public function exec_insert(){
        try {
            //insertする値の整形
            $aryBindVal['LastUpdate'] = date('Y-m-d H:i:s');
            $aryBindVal['Contact'] = $this->aryParam['input_form']['contact'];
            $aryBindVal['Name'] = $this->aryParam['input_form']['your_name'];
            $aryBindVal['Kana'] = $this->aryParam['input_form']['your_name_kana'];
            $aryBindVal['Zip'] = $this->aryParam['input_form']['zip_code'];
            $aryBindVal['Address'] = $this->aryParam['input_form']['address'];
            $aryBindVal['Tel'] = $this->aryParam['input_form']['tel'];
            $aryBindVal['EMail'] = $this->aryParam['input_form']['email'];
            $aryBindVal['BirthYear'] = $this->aryParam['input_form']['birth_yyyy'];
            $aryBindVal['BirthMonth'] = $this->aryParam['input_form']['birth_mm'];
            $aryBindVal['BirthDay'] = $this->aryParam['input_form']['birth_dd'];
            $aryBindVal['Sex'] = $this->aryParam['input_form']['optionsSex'];

            $this->objDb->executeInsert('contact_db.Contact', $this->aryContactCol, $aryBindVal);
        }
        catch(exception $e)
        {
            throw $e;
        }
    }
}