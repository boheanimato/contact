<?php
//-----------------------------------------------
// Action Map用定義
//-----------------------------------------------

class ControlDefine
{
    public static $aryControlDef = array
    (
        'contact'             => array(
            'form'                        => array('ContactHandle'                          , 'お問い合わせ入力'),
            'confirm'                     => array('ContactHandle'                        , 'お問い合わせ入力確認'),
            'complete'                    => array('ContactHandle'                        , 'お問い合わせ完了'),
        ),
    );
}
?>