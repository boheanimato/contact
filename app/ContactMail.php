<?php
/**
 * ContactMail
 *
 * このクラスではメール送信をします
 *
 *
 * @author ashibuya
 */

class ContactMail {

    const FROM_OFFICE_NAME ='Dummy Office';
    const MAIL_OFFICE = 'boheanimato@gmail.com';
    private $aryParam = array();

    public function __construct($aryReq)
    {
        $this->aryParam = $aryReq;
    }

    /**
     * send_contact_to_customer
     * @access public
     * @return void
     */
    public function send_contact_to_customer(){
        $header="";
        $body="";

        $mail_adddress = $this->aryParam['input_form']['email'];
        $name= $this->aryParam['input_form']['your_name'];

        $header='From:' .mb_encode_mimeheader(self::FROM_OFFICE_NAME) .'<'.self::MAIL_OFFICE.'>';

        $subject="お問い合わせありがとうございます ";

        $body.=$name." 様\r\n\r\n";
        $body.="当社ウェブサイトよりお問い合わせ頂きありがとうございます。\r\n";
        $body.="折り返し弊社担当より24時間以内にご連絡を差し上げますので今しばらくお待ちください。\r\n";
        $body.="（休業日に当たる場合は翌営業日のお返事になります)\r\n\r\n";
        $body.=$this->_get_contact_body();

        mb_send_mail($mail_adddress , $subject  ,  $body,  $header);

    }

    /**
     * send_orderdone_to_office
     * @access public
     * @return void
     */
    public function send_orderdone_to_office(){
        $header="";
        $body="";

        $mail_adddress = self::MAIL_OFFICE;
        $name= $this->aryParam['input_form']['your_name'];

        $header='From:' .mb_encode_mimeheader(self::FROM_OFFICE_NAME) .'<'.self::MAIL_OFFICE.'>';

        $subject="問い合わせがはいりました ";

        $body.=$this->_get_contact_body();

        mb_send_mail($mail_adddress , $subject  ,  $body,  $header);

    }

    private function _get_contact_body(){
        $body='';

        $body ="-------------\r\n";
        $body.="<お問い合わせ内容>\r\n";
        $body.= "お問い合わせ内容：". $this->aryParam['input_form']['contact']."\r\n";
        $body.= "お名前：".$this->aryParam['input_form']['your_name']."\r\n";
        $body.= "フリガナ：".$this->aryParam['input_form']['your_name_kana']."\r\n";
        $body.= "郵便番号：".$this->aryParam['input_form']['zip_code']."\r\n";
        $body.= "ご住所：".$this->aryParam['input_form']['address']."\r\n";
        $body.= "お電話番号：".$this->aryParam['input_form']['tel']."\r\n";
        $body.= "メールアドレス：".$this->aryParam['input_form']['email']."\r\n";
        $body.= "生年月日：".$this->aryParam['input_form']['birth_yyyy']."年".$this->aryParam['input_form']['birth_mm']."月".$this->aryParam['input_form']['birth_dd']."日"."\r\n";

        $body.= "性別：".$this->aryParam['input_form']['optionsSex']."\r\n";

        return $body;
    }

}