<?php
require_once( APP_DIR .'FrontBase.php');
require_once( FW_DIR . 'ValidationManager.inc');
require_once( APP_DIR .'ContactTable.php');
require_once( APP_DIR .'ContactMail.php');

/**
 * ContactHandle
 *
 *
 * @author ashibuya
 */

class ContactHandle  extends FrontBase{

    private $aryReq = array();
    /**
     * __construct
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->aryReq = $this->objReq->getAll();
    }

    /**
     * 登録実行
     *
     * @access public
     * @return void
     */
    public function execute()
    {
        $strTemplate = '';
        $aryErr = array();

        //カテゴリの決定
        $strUri = $this->objReq->getUri();
        $aryUrl = explode('/',$strUri);
        $strCategory = isset($aryUrl[2])?$aryUrl[2]:'';

        error_log( $strCategory);



        switch($strCategory)
        {
            //----------------------------------------
            // 登録
            //----------------------------------------
            case 'form':
                $this->_initForInput();
                $strTemplate = 'ContactForm.inc';
                break;
            //----------------------------------------
            // 確認
            //----------------------------------------
            case 'confirm':
                // validate
                $aryErr = $this->_doValidate();

                if(!empty($aryErr)){
                    $this->objView->assign('aryErr', $aryErr);
                    // エラーがあったらInput画面
                    $this->_initForInput();
                    $strTemplate = 'ContactForm.inc';
                }
                else {
                    $this->_editView();
                    $strTemplate = 'ContactConfirm.inc';
                }
                break;

            case 'complete':
                try{
                    //データベース登録
                    $objTable = new ContactTable($this->aryReq);
                    $objTable->exec_insert();
                }catch(Exception $e){
                    error_log($e->getMessage());
                    //エラー発生
                    error_log("Contact data insert ERROR!");
                    //TODO:管理者にメール送信

                }
                //データベース登録はできなかったけど、メールは送信する
                //お問い合わせメール送信
                $objMail = new ContactMail($this->aryReq);
                $objMail->send_contact_to_customer();
                $objMail->send_orderdone_to_office();

                //TODO：エラーがおきても完了している
                $strTemplate = 'ContactComplete.inc';

                break;
            default:
                break;

        }

        // エラーメッセージ
        $this->objView->setAttribute('aryErr'   , $aryErr);
        // テンプレート
        $this->objView->setTmplateName($strTemplate);
        $this->objView->nativeShow();
    }

    private function _initForInput(){

        // テンプレートアサイン
        $this->objView->assign('aryMonth', array(
                ''=>'',
                01 => '01',
                02 => '02',
                03 => '03',
                04 => '04',
                05 => '05',
                06 => '06',
                07 => '07',
                08 => '08',
                09 => '09',
                10 => '10',
                11 => '11',
                12 => '12'
            )
        );
        $this->objView->assign('aryDay', array(
                ''=>'',
                01 => '01',
                02 => '02',
                03 => '03',
                04 => '04',
                05 => '05',
                06 => '06',
                07 => '07',
                08 => '08',
                09 => '09',
                10 => '10',
                11 => '11',
                12 => '12',
                13 => '13',
                14 => '14',
                15 => '15',
                16 => '16',
                17 => '17',
                18 => '18',
                19 => '19',
                20 => '20',
                21 => '21',
                22 => '22',
                23 => '23',
                24 => '24',
                25 => '25',
                26 => '26',
                27 => '27',
                28 => '28',
                29 => '29',
                30 => '30',
                31 => '31'
            )
        );
    }
    /**
     * execute
     * @access public
     * @return void
     */
    private function _doValidate(){
        $aryInput = array();
        $aryInput=$this->aryReq['input_form'];
        $strBirthday = $aryInput['birth_yyyy'].$aryInput['birth_mm'].$aryInput['birth_dd'];
        $aryInput['birthday']= $strBirthday;

        //Validation
        $objVd = new ValidationManager($aryInput);

        $ary_required_field=array(
            'your_name'         =>'お名前（漢字）' ,
            'your_name_kana'    =>'お名前(フリガナ)' ,
            'tel'               =>'お電話番号' ,
            'email'             =>'メールアドレス' ,
            'contact'           =>'お問い合わせ内容' ,
            'optionsSex'           =>'性別' ,

        );

        $objVd->setFieldRule( $ary_required_field , array( 'required' ));
        $objVd->setFieldRule( array('email' =>'メールアドレス' ), array( 'email'));
        $objVd->setFieldRule( array('tel' =>'お電話番号' ,'zip_code' =>'郵便番号', 'birthday' =>'生年月日'), array( 'digit'));
        $objVd->setFieldRule( array('your_name' =>'お名前（漢字）' ), array( 'maxlength' =>array('length' => 20 )));
        $objVd->setFieldRule( array('your_name_kana' =>'お名前（フリガナ）' ), array( 'maxlength' =>array('length' => 60 )));

        $ary_error = $objVd->get_errors();

        return $ary_error;
    }

    private function _editView(){

        $sex_code=$this->aryReq['input_form']['optionsSex'];
        $this->objView->assign('sex_name',  Config::$v['sex'][$sex_code]);
        $this->objView->assign('birthday',  $this->aryReq['input_form']['birth_yyyy'].'年'.$this->aryReq['input_form']['birth_mm'].'月'.$this->aryReq['input_form']['birth_dd'].'日');

    }

}