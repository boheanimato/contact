
$(function(){

    // 郵便番号入力による住所自動入力
    $('#zip_code').change(function(){

        var zip = $(this).val();
        var url = 'http://api.zipaddress.net?callback=?';
        var query = {'zipcode': zip};
        $.getJSON(url, query, function(){

        })
        .success(function(json) {
                if(json.code == "400"){
                    $('#address').val('');
                }else{
                    $('#address').val(json.pref +json.address );
                }

            })
        .error(function() {$('#address').val(''); })

    });

});
