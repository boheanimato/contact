<?php
/**
 *
 *  フロントエンドエントリポイント
 *
 * アプリディレクトリを指定して、実行クラスを決定します。
 *
 * ログの出力レベルをここで変更します。
 * 必要なフレームワークライブラリはここでrequireします。
 *
 *
 * @author     ashibuya
 *
 */


//-----------------------
//Application setting
//-----------------------
$strCurrentDir = dirname(__FILE__);
$strHtdocsDir = dirname($strCurrentDir);
$strBaseDir = dirname($strHtdocsDir)."/contact";
//-----------------------
// change the following paths if necessary
//-----------------------
define("APP_DIR"        , $strBaseDir . "/app/");
define("FW_DIR"        , $strBaseDir . "/fw/");
define("LOG_DIR"        , $strBaseDir . "/logs/");
define("EX_DIR"         , $strBaseDir . "/ex/");
define("VIEW_DIR"       , $strBaseDir . "/smarty/");

/**
 * Main Exec
 * クラスの決定と呼びだし
 *
 */
//----------------------------------
//クラス、プロパティ、カテゴリの決定
//----------------------------------
require_once(FW_DIR . "BasicFormatter.inc");
require_once(FW_DIR . "WebRequest.inc");
require_once(FW_DIR . "ViewManager.inc");
require_once(FW_DIR . "DatabaseManager.inc");
require_once(FW_DIR . "ClassControlManager.inc");
require_once(APP_DIR.'ControlDefine.inc');
require_once(APP_DIR.'Config.inc');
//require_once(APP_DIR.'BasicFormatter.inc');


$objReq = new WebRequest();
$strUri = $objReq->getUri();
$aryUrl = explode('/',$strUri);

$strAction = isset($aryUrl[1])?$aryUrl[1]:'';
$strCategory = isset($aryUrl[2])?$aryUrl[2]:'';

error_log($strAction);
error_log($strCategory);

//クラスの決定
ClassControlManager::getInstance($strAction,$strCategory);
ClassControlManager::setClass();
$strClass = ClassControlManager::getClass();


if(!file_exists(APP_DIR . $strClass . ".php"))
{
    //Not Found
    $strClass = 'NotFound';
}
require_once( APP_DIR . $strClass . ".php");
$objClass = new $strClass();
$objClass->execute();


