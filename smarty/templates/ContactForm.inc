<<html lang="ja">
{*---------- Header Start ----------*}
{include file='include/head.inc'}
{*---------- Header End ----------*}
<body>

{*---------- Contents Start ----------*}
<div class="container"> <!-- start container -->
    <div class="row"><!-- start row -->
        <form method="post" class="form-horizontal" action="/contact/confirm">
            <input type="hidden" id="_wpnonce" name="_wpnonce" value="555d273b17">
            <input type="hidden" id="post_method" name="post_method" value="Y">
            <div class="col-md-offset-2 col-md-8 col-xs-12">
                <h1><i class="fa fa-comments-o"></i>お問い合わせ</h1>


                <div class="category-user">
                    <div class="form-group">
                        <label for="inputContact" class="control-label ">お問い合わせ内容<span class="required-mark">必須</span></label>
                        <p class="bg-danger">{$aryErr.contact|default:''}</p>
                        <textarea rows="5" class="form-control input-lg" name="input_form[contact]" required>{$smarty.request.input_form.contact|default:''}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="inputName" class="control-label ">お名前（漢字）<span class="required-mark">必須</span></label>
                        <p class="bg-danger">{$aryErr.your_name|default:''}</p>
                        <input type="text" name='input_form[your_name]' class="form-control input-lg" placeholder="山田 太郎" value='{$smarty.request.input_form.your_name|default:''}' required>

                        <span class="help-block">20文字以内で入力してください。</span>
                    </div>
                    <div class="form-group">
                        <label for="inputHurigana" class="control-label">お名前(フリガナ)<span class="required-mark">必須</span></label>
                        <p class="bg-danger">{$aryErr.your_name_kana|default:''}</p>
                        <input type="text" name="input_form[your_name_kana]" class="form-control input-lg" placeholder="ヤマダ タロウ" value='{$smarty.request.input_form.your_name_kana|default:''}' required>
                        <span class="help-block">60文字以内で入力してください。</span>
                    </div>
                    <div class="form-group">
                        <label for="inputZip" class="control-label">郵便番号<span class="info-mark">ハイフン不要</span></label>
                        <p class="bg-danger">{$aryErr.zip_code|default:''}</p>
                        <input type="text" name="input_form[zip_code]" id="zip_code" class="form-control input-lg" placeholder="0000000" value='{$smarty.request.input_form.zip_code|default:''}'>
                    </div>
                    <div class="form-group">
                        <label for="inputAddress" class="control-label">住所</label>
                        <p class="bg-danger">{$aryErr.address|default:''}</p>
                        <input type="text" name="input_form[address]" id="address" class="form-control input-lg" placeholder="" value='{$smarty.request.input_form.address|default:''}'>
                    </div>
                    <div class="form-group">
                        <label for="inputMail" class="control-label">お電話番号<span class="info-mark">ハイフン不要</span><span class="required-mark">必須</span></label>
                        <p class="bg-danger">{$aryErr.tel|default:''}</p>
                        <input type="text" name="input_form[tel]" class="form-control input-lg" placeholder="" value='{$smarty.request.input_form.tel|default:''}' required>
                        <span class="help-block">半角数字で入力してください。</span>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="control-label">メールアドレス<span class="required-mark">必須</span></label>
                        <p class="bg-danger">{$aryErr.email|default:''}</p>
                        <input type="email" name="input_form[email]" class="form-control input-lg" placeholder="sample@exsample.com" value='{$smarty.request.input_form.email|default:''}' required>
                    </div>


                    <div class="form-group">
                        <label class="control-label"  for="birth_yyyy">生年月日 </label>
                        <p class="bg-danger">{$aryErr.birthday|default:''}</p>
                        <div class="form-inline">

                            <input type="text" name="input_form[birth_yyyy]" id="birth_yyyy" value='{$smarty.request.input_form.birth_yyyy|default:''}' class="form-control input-lg col-xs-1" placeholder="" > <span>年</span>
                            {html_options  class="form-control input-lg"  name='input_form[birth_mm]' options=$aryMonth selected=$optionMonthSelected|default:''}月
                            {html_options  class="form-control input-lg"  name='input_form[birth_dd]' options=$aryDay selected=$optionDaySelected|default:''}日

                        </div>
                    </div>

                    <div class="form-group">
                        <label for="radioOption1" class="control-label">性別<span class="required-mark">必須</span></label>
                        <p class="bg-danger">{$aryErr.optionsSex|default:''}</p>
                        <div class="form-inline">
                            <div class="radio">
                                <label class=" input-lg">
                                    <input type="radio" name="input_form[optionsSex]" value="1" />男</label>
                            </div>
                            <div class="radio">
                                <label  class=" input-lg">
                                    <input type="radio" name="input_form[optionsSex]" value="2" />女</label>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="form-group">
                    <button type="submit"  value="確認" id="formbtn" data-loading-text="Loading..." class="btn btn-lg btn-block  btn-primary btn-basic"><i class="fa fa-chevron-right"></i> 入力内容確認</button>
                </div>
        </form>
    </div>
</div><!-- end row -->
</div><!-- end container -->
{*---------- Contents End ----------*}

{*---------- Footer Start ----------*}
{include file='include/footer.inc'}
{*---------- Footer End ----------*}

{*---------- Footer JS  Start ----------*}
{include file='include/footer_js.inc'}
{*---------- Footer JS End ----------*}
</body>
</html>
