<html lang="ja">
{*---------- Header Start ----------*}
{include file='include/head.inc'}
{*---------- Header End ----------*}
<body>

{*---------- Contents Start ----------*}
<div class="container"> <!-- start container -->
    <div class="row"><!-- start row -->
        <form method="post" class="form-horizontal" action="complete">
            <input type="hidden" id="_wpnonce" name="_wpnonce" value="555d273b17">
            <div class="col-md-offset-2 col-md-8 col-xs-12">
                <h1><i class="fa fa-comments-o"></i>お問い合わせ</h1>
                    <div class="category-user">
                        <p>お問いあわせいただき、ありがとうございました。<br>追って担当者からご連絡させていただきます。</p>
                    <div>
            </div>
    </div><!-- end row -->
</div><!-- end container -->
{*---------- Contents End ----------*}

{*---------- Footer Start ----------*}
{include file='include/footer.inc'}
{*---------- Footer End ----------*}

{*---------- Footer JS  Start ----------*}
{include file='include/footer_js.inc'}
{*---------- Footer JS End ----------*}
</body>
</html>