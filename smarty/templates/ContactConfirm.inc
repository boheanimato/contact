<html lang="ja">
{*---------- Header Start ----------*}
{include file='include/head.inc'}
{*---------- Header End ----------*}
<body>

{*---------- Contents Start ----------*}
<div class="container"> <!-- start container -->
    <div class="row"><!-- start row -->
        <form method="post" class="form-horizontal" action="/contact/complete">
            <input type="hidden" id="_wpnonce" name="_wpnonce" value="555d273b17">
            <div class="col-md-offset-2 col-md-8 col-xs-12">
                <h1><i class="fa fa-comments-o"></i>お問い合わせ</h1>
                <div class="category-user">
                    <div class="form-group">
                        <label for="inputContact" class="control-label ">お問い合わせ内容</label>
                        <p class="input-lg">{$smarty.request.input_form.contact}</p>
                        <input type="hidden"  name="input_form[contact]" value="{$smarty.request.input_form.contact}">
                    </div>
                    <div class="form-group">
                        <label for="inputName" class="control-label ">お名前（漢字）</label>
                        <p class="input-lg">{$smarty.request.input_form.your_name}</p>
                        <input type="hidden"  name="input_form[your_name]" value="{$smarty.request.input_form.your_name}">
                    </div>
                    <div class="form-group">
                        <label for="inputHurigana" class="control-label">お名前(フリガナ)</label>
                        <p class="input-lg">{$smarty.request.input_form.your_name_kana}</p>
                        <input type="hidden"  name="input_form[your_name_kana]" value="{$smarty.request.input_form.your_name_kana}">
                    </div>
                    <div class="form-group">
                        <label for="inputZip" class="control-label">郵便番号</label>
                        <p class="input-lg">{$smarty.request.input_form.zip_code}</p>
                        <input type="hidden"  name="input_form[zip_code]" value="{$smarty.request.input_form.zip_code}">
                    </div>
                    <div class="form-group">
                        <label for="inputAddress" class="control-label">住所</label>
                        <p class="input-lg">{$smarty.request.input_form.address}</p>
                        <input type="hidden"  name="input_form[address]" value="{$smarty.request.input_form.address}">
                    </div>
                    <div class="form-group">
                        <label for="inputMail" class="control-label">お電話番号</label>
                        <p class="input-lg">{$smarty.request.input_form.tel}</p>
                        <input type="hidden"  name="input_form[tel]" value="{$smarty.request.input_form.tel}">
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="control-label">メールアドレス</label>
                        <p class="input-lg">{$smarty.request.input_form.email}</p>
                        <input type="hidden"  name="input_form[email]" value="{$smarty.request.input_form.email}">
                    </div>
                    <div class="form-group">
                        <input type="hidden"  name="input_form[birth_yyyy]" value="{$smarty.request.input_form.birth_yyyy}">
                        <input type="hidden" name="input_form[birth_mm]" value="{$smarty.request.input_form.birth_mm}">
                        <input type="hidden" name="input_form[birth_dd]" value="{$smarty.request.input_form.birth_dd}">
                        <label class="control-label"  for="birth_yyyy">生年月日 </label>
                        <p class="input-lg">{$birthday}</p>
                    </div>

                    <div class="form-group">
                        <label for="radioOption1" class="control-label">性別</label>
                        <input type="hidden" id="optionsSex" name="optionsSex" value="female">

                        <p class="input-lg">{$sex_name}</p>
                        <input type="hidden"  name="input_form[optionsSex]" value="{$smarty.request.input_form.optionsSex}">
                    </div>
                    </div>

                <div class="form-group">
                    <button type="submit"  value="確認" id="formbtn" data-loading-text="Loading..." class="btn btn-lg btn-block  btn-primary btn-basic"><i class="fa fa-chevron-right"></i> 送信</button>
                    <input type="button"  value="もどる" id="formbtn" data-loading-text="Loading..." class="btn btn-lg  btn-default btn-basic" onclick="history.back()"></button>
                </div>
        </form>
    </div>
</div><!-- end row -->
</div><!-- end container -->
{*---------- Contents End ----------*}

{*---------- Footer Start ----------*}
{include file='include/footer.inc'}
{*---------- Footer End ----------*}

{*---------- Footer JS  Start ----------*}
{include file='include/footer_js.inc'}
{*---------- Footer JS End ----------*}
</body>
</html>