<?php
/**
 * Created by PhpStorm.
 * User: ashibuya
 * Date: 2015/10/01
 * Time: 8:59
 */

class ValidationManager{

    private $ary_value = array();
    private $ary_errors = array();

    public function __construct($input){
        $this->ary_value = $input;
    }

    public function setFieldRule($aryFieldName, $aryRule){

        foreach($aryRule as $rule){

            switch($rule){
                case 'required':
                    $this->_validate_required($aryFieldName);
                    break;

                case 'email':
                    $this->_validate_email($aryFieldName);
                    break;

                case 'digit':
                    $this->_validate_digit($aryFieldName);
                    break;

                case 'maxlength':
                    $this->_validate_maxlength($aryFieldName, $rule['length']);
                    break;

            }
        }

    }

    public function get_errors(){
        return $this->ary_errors;
    }

    /*
    * private method
    */

    private function _validate_required($aryFieldName)
    {
        foreach($aryFieldName as $field=> $fieldName){
            if( !isset($this->ary_value[$field]) ||  $this->ary_value[$field] == ''){

                //array_push($this->ary_errors, array( $field,  sprintf(' %sは必須項目です。入力してください。' ,$fieldName )));
                $this->ary_errors[$field] = sprintf(' %sは必須項目です。入力してください。', $fieldName);
            }
        }
    }

    private function _validate_email($aryFieldName)
    {
        foreach($aryFieldName as $field=> $fieldName) {
            //簡単な正規表現
            if (!preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $this->ary_value[$field])){
                //E-Mail形式入力ではない為、エラー
                //array_push($this->ary_errors, array($field, sprintf(' %sはEmailアドレスです。正しく入力してください。', $fieldName)));
                $this->ary_errors[$field] = sprintf(' %sはEmailアドレスです。正しく入力してください。', $fieldName);
            }
        }

    }

    private function _validate_digit($aryFieldName)
    {
        foreach($aryFieldName as $field=> $fieldName) {
            if (!preg_match("/^[0-9]+$/", $this->ary_value[$field])){
            //if (!mb_ereg("/^[a-zA-Z0-9]+$/", $this->ary_value[$field])) {
                $this->ary_errors[$field] =sprintf(' %sは半角数字で入力してください。', $fieldName);
            }
        }
    }

    private function _validate_maxlength($aryFieldName, $length)
    {
        foreach($aryFieldName as $field=> $fieldName) {
            $checkParamLength = mb_strlen($this->ary_value[$field], 'UTF-8');

            if ($checkParamLength > $length) {
                //array_push($this->ary_errors, array($field, sprintf(' %sは%d文字以下で入力してください。', $fieldName, $length)));
                $this->ary_errors[$field] = sprintf(' %sは%d文字以下で入力してください。', $fieldName, $length);
            }
        }
    }

}
?>