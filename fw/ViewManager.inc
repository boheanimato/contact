<?php
include_once(EX_DIR . 'libs/Smarty.class.php');


/**
 * ViewGeneralManager  Smartyラッパークラス
 *
 * @uses Smarty
 * @author ashibuya
 */
class ViewManager extends Smarty
{
    protected $aryContents = array();
    protected $includeView = null;
    protected $includeDir  = null;
    protected $mainView    = 'main.tpl';

    const SMARTY_EXTENSION = '.tpl';

    /**
     * __construct Smartyディレクトリをメンバ変数にセット
     *
     * @access public
     * @return void
     */
    public function  __construct()
    {
        parent::__construct();

    }

    public function setDirectory($extra='')
    {
        $this->setTemplateDir(VIEW_DIR.'templates'.$extra);
        $this->setCompileDir(VIEW_DIR.'templates_c'.$extra);
        $this->setCacheDir(VIEW_DIR.'cache'.$extra);
        $this->setConfigDir(VIEW_DIR.'configs'.$extra);
        $this->addPluginsDir(VIEW_DIR.'plugins');
    }


    /**
     * show アサインするテンプレートを設定し、Smartyのdisplayメソッドをコールする
     *
     * @access public
     * @return void
     */
    public function show()
    {
        $this->_escapeParams();
        $this->assign('incview', $this->includeView);
        $this->display($this->mainView);
    }

    /**
     * getShowContents アサインするテンプレートを設定し、Smartyのfetchメソッドをコールする
     *
     * @access public
     * @return void
     */
    public function getShowContents()
    {

        $this->assign('incview', $this->includeView);
        return $this->fetch($this->mainView);
    }

    /**
     * nativeShow 指定テンプレートをそのままにSmartyのdisplayメソッドをコールする
     *
     * @access public
     * @return void
     */
    public function nativeShow()
    {
        $this->_escapeParams();
        $this->assign('_templatename', $this->includeView);
        $this->display($this->includeView);
    }

    /**
     * setAttribute Smartyにアサインするキーをセットします
     *
     * @param string $name  アサインするキー
     * @param string $value アサインする値
     * @access public
     * @return void
     */
    public function setAttribute($name, $value)
    {
        $this->assign($name, BasicFormatter::_getHtmlspecialchars($value));
    }

    /**
     * setAryAttribute Smartyにアサインするキーと値を配列でセットします
     *
     * @param array $arValue アサインキーと値の連想配列
     * @access public
     * @return void
     */
    public function setAryAttribute($aryValue)
    {
        foreach($aryValue as $_key => $_val)
        {
            $this->setAttribute($_key, $_val);
        }
    }

    /**
     * setTmplateName 表示するテンプレート名をセットします
     *
     * @param string $tplname 表示テンプレート名
     * @access public
     * @return void
     */
    public function setTmplateName($includeView)
    {
        $this->includeView = $includeView;
    }

    /**
     * setMainView 表示するメインテンプレートを設定します
     *
     * @param string $mainView 表示するメインテンプレート
     * @access public
     * @return void
     */
    public function setMainView($mainView)
    {
        $this->mainView = $mainView;
    }

    /**
     * _escapeParams リクエストパラメータをエスケープする
     *
     * @access private
     * @return void
     */
    private function _escapeParams()
    {
        //----------------------------------
        //リクエストパラメータをエスケープする
        //----------------------------------
        //$_REQUEST
        if (get_magic_quotes_gpc()){
            $_REQUEST = stripslashes($_REQUEST);
        }
        $_REQUEST = BasicFormatter::_getHtmlspecialchars($_REQUEST);
        //$_POST
        if (get_magic_quotes_gpc()){
            $_POST = stripslashes($_POST);
        }
        $_POST = BasicFormatter::_getHtmlspecialchars($_POST);
        //$_GET
        if (get_magic_quotes_gpc()){
            $_GET = stripslashes($_GET);
        }
        $_GET = BasicFormatter::_getHtmlspecialchars($_GET);
    }
}
?>
