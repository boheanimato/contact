<?php
/**
 * ClassControlManager クラス決定
 *
 * @author     ashibuya
 */
class ClassControlManager
{
    protected static $instance = null;

    protected static $action    = '';
    protected static $category  ='';
    protected static $className          ='';

    //ScriptUrl
    const CLASS_ACTION_DEFAULT      ='_default';
    const CLASS_CATEGORY_DEFAULT    ='_default';

    /**
     * インスタンス作成
     *
     * @access public static
     * @param array $aryHost
     * @return instance
     */
    public static function getInstance( $action, $category)
    {
        if(!isset(self::$instance))
        {
            $class = __CLASS__;
            self::$instance = new $class( $action, $category);
        }
        return self::$instance;
    }

    public function __construct( $action, $category)
    {
        self::$action       = $action==''?self::CLASS_ACTION_DEFAULT:$action;
        self::$category     = $category==''?self::CLASS_CATEGORY_DEFAULT:$category;
    }
    /**
     * アクション取得
     *
     * @access public
     * @return string action
     */
    public function getAction()
    {
        return self::$action;

    }
    /**
     * カテゴリ取得
     *
     * @access public
     * @return string action
     */
    public function getCategory()
    {
        return self::$category;

    }

    /**
     * アクション設定
     *
     * @access public
     * @return string action
     */
    public function setAction($strAction)
    {
        self::$action= $strAction;

    }
    /**
     * カテゴリ設定
     *
     * @access public
     * @return string action
     */
    public function setCategory($strCategory)
    {
        self::$category = $strCategory;

    }

    /**
     * getArrayDef
     *
     * @access public
     * @return array Define
     */
    public function getDef()
    {
        $ary = array();
        if($this->has($this->property))
        {
            $ary = ControlDefine::$aryControlDef[$this->action];
        }
        return $ary;
    }

    /**
     * getClass
     *
     * @access public
     * @return string class
     */
    public function setClass()
    {
        $class = "";
        error_log('Action   :'.self::$action);
        error_log('Category :'.self::$category);

        if(isset( ControlDefine::$aryControlDef[self::$action]))
        {
            $ary = ControlDefine::$aryControlDef[self::$action];

            //クラスの決定
            if(array_key_exists(self::$category , $ary))
            {
                $class = $ary[self::$category][0];
            }
        }
        self::$className = $class;
    }

    /**
     * getClass
     *
     * @access public
     * @return string class
     */
    public function getClass()
    {
        return self::$className;
    }

    /**
     * has
     *
     * @access public
     * @return void
     */
    public function has()
    {
        return array_key_exists($this->action, ControlDefine::$aryControlDef);
    }
}
?>
