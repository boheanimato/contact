<?php
/**
 * DatabaseManager
 *
 * このクラスではDB操作をします
 *
 *
 * @author ashibuya

 */

class DatabaseManager {

    private $objCon = null;

    private $aryDef= array('dns'=>'mysql:host=127.0.0.1;port=3306'
                            ,'user'=>'root'
                            ,'password'=>'root'
                            ,'pdo_option'=>array( ));


    public function __construct()
    {
        try {

            $this->objCon= null;
            $this->objCon  =new PDO(
                $this->aryDef["dns"],
                $this->aryDef["user"],
                $this->aryDef["password"],
                $this->aryDef["pdo_option"]);
            $this->objCon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->objCon->setAttribute(PDO::ATTR_TIMEOUT, 5 );
        }
        catch(exception $e)
        {
            throw $e;
        }

    }
    /**
     * executeInsert
     *
     * @return void
     */
    public function executeInsert( $strTagetDbTable,  $aryInsertColumn, $aryBindValue)
    {
        try
        {
            $strTableColumn = implode(',',$aryInsertColumn);
            $strBindColumn = ':' . implode(',:',$aryInsertColumn);
            $sql
                = 'insert into '
                .$strTagetDbTable
                .'('
                .$strTableColumn
                .') values ('
                .$strBindColumn
                .')';

            error_log($sql);
            $stmt = $this->objCon->prepare($sql);


            $this->parameterBind($stmt, $aryBindValue);
            $stmt->execute();

            $count = $stmt->rowCount();
            error_log($count);

            $stmt->closeCursor();

        }
        catch(exception $e)
        {
            throw $e;
        }
    }

    /**
     * parameterBind    パラメーターをバインドする
     *
     * @param mixed $obj
     * @param mixed $aryBindValue
     * @access protected
     * @return void
     */
    private function parameterBind($obj, $aryBindValue){

        error_log(print_r($aryBindValue, true));
        foreach($aryBindValue as $columnName =>$value)
        {

            $dbBind = ':'.$columnName;
            $obj->bindValue( $dbBind, $value);
            //error_log($dbBind. ' ==> ' . $value);
        }
    }

}