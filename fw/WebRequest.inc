<?php
/**
 * WebRequest
 *
 * このクラスではWEBのリクエスト情報を管理します
 * インプットされたリクエストパラメータをパースしてパラメータとして保持します
 *
 * @author ashibuya

 */
class WebRequest
{
    protected $aryServerInfo = array();
    protected $actionPath = null;
    protected $aryParameter = array();
    protected $requestParams= array();
    protected $getParams = array();
    protected $postParams = array();
    protected $getServer = array();

    /**
     * initialize
     *
     * @param mixed $context
     * @param array $parameters
     * @access public
     * @return void
     */
    public function __construct($default=true)
    {
        if($default)
        {
            $this->_loadParameters();
        }
    }

    /**
     * getMethodName
     *
     * @access public
     * @return void
     */
    public function getMethodName()
    {
      return isset($this->aryServerInfo['REQUEST_METHOD']) ? $this->aryServerInfo['REQUEST_METHOD'] : 'GET';
    }

    /**
     * set
     *
     * @param array $aryParam
     * @access public
     * @return void
     */
    public function set($aryParam)
    {
        foreach ($aryParam as $key =>$value)
        {
            if(!isset($this->aryParameter[$key])){
                $this->aryParameter[$key] = $value;
            }
        }
    }

    /**
     * get
     *
     * @param string $name
     * @access public
     * @return mixed
     */
    public function  & get($name)
    {
        $value = & $this->aryParameter[$name];
        return $value;
    }

    /**
     * replace
     *
     * @param array $aryParam
     * @access public
     * @return void
     */
    public function replace($aryParam)
    {
        foreach ($aryParam as $key =>$value)
        {
            if(isset($this->aryParameter[$key])){
                $this->aryParameter[$key] = $value;
            }
        }
    }

    /**
     * _setRequestParams
     *
     * @access private
     * @return array
     */
    protected function _setRequestParams(){
        if (!$this->requestParams){
            if (get_magic_quotes_gpc()){
                $_REQUEST = stripslashes($_REQUEST);
            }
            $this->requestParams = BasicFormatter::_getStrip_tags($_REQUEST);
            //$this->requestParams = $_REQUEST;
        }
        return $this->requestParams;
    }

    /**
     * _setPostParams
     *
     * @access private
     * @return array
     */
    protected function _setPostParams(){
        if (!$this->postParams){
            if (get_magic_quotes_gpc()){
                $_POST = stripslashes($_POST);
            }
            $this->postParams = BasicFormatter::_getStrip_tags($_POST);

            //$this->postParams = $_POST;

        }
        return $this->postParams;
    }

    /**
     * _setGetParams
     *
     * @access private
     * @return array
     */
    protected function _setGetParams()
    {
        // merge GET parameters
        if (!$this->getParams)
        {
            if (get_magic_quotes_gpc())
            {
                $_GET = stripslashes($_GET);
            }
            //$this->getParams = $_GET;
            //
            $this->getParams = BasicFormatter::_getStrip_tags($_GET);
        }
        return $this->getParams;
    }

    /**
     * _loadParameters
     *
     * パラメータのロード
     *
     * @access public
     * @return void
     */
    private function _loadParameters()
    {
        // merge GET parameters
        $this->set($this->_setGetParams());
        // merge POST parameters
        $this->set($this->_setPostParams());
        // set SERVER parameters
        $this->_getServerInfoArray();
    }

    /**
     * _getPathInfo
     *
     * クエリストリングより前のパス情報を取得する
     *
     * @param mixed $filter
     * @access private
     * @return void
     */
    private function _getPathInfo()
    {
        $pathInfo = '';
        $requestURI = $this->getUri();
        $queryString = $this->getQueryString();
        if(isset($requestURI))
        {
            $iPos = strpos($requestURI, '?');
            if(!$iPos)
            {
                $pathInfo= substr($requestURI, 1);
            }
            else
            {
                $pathInfo = substr($requestURI, 1, $iPos-1);
            }
        }
        if(!$pathInfo)
        {
            $pathInfo = '/';
        }
        return $pathInfo;
    }

    /**
     * _getServerInfoArray
     *
     * サーバ変数をメンバ配列に格納する
     *
     * @access protected
     * @return void
     */
    private function _getServerInfoArray()
    {
        if (!$this->aryServerInfo)
        {
             $aryTmp =& $_SERVER;
        }

        foreach($aryTmp as $key=>$val)
        {
            $val = htmlspecialchars($aryTmp[$key]);
            $this->aryServerInfo[$key] = $val;
        }

        return $this->aryServerInfo;
    }

    /**
     * getUriPrefix
     *
     * URI Prifixを返却
     *
     * @access public
     * @return void
     */
    public function getUriPrefix()
    {
        if ($this->isSecure())
        {
            $standardPort = '443';
            $proto = 'https';
        }
        else
        {
            $standardPort = '80';
            $proto = 'http';
        }
        $port = $this->aryServerInfo['SERVER_PORT'] == $standardPort || !$this->aryServerInfo['SERVER_PORT'] ? '' : ':'.$this->aryServerInfo['SERVER_PORT'];
        return $proto.'://'.$this->aryServerInfo['SERVER_NAME'].$port;
    }

    /**
     * isSecure
     *
     * HTTPSプロトコルかどうか
     *
     * @access public
     * @return void
     */
    public function isSecure()
    {
        if(isset($this->aryServerInfo['HTTPS']) && strtolower($this->aryServerInfo['HTTPS']) == 'on')
        {
            return true;
        }
        else
        {
            return false;
        }
      }

    /**
    * getReferer
    *
    * リファラーを返却する
    *
    * @param mixed $filter
    * @access public
    * @return void
    */
     public function getReferer()
     {
         return isset($this->aryServerInfo['HTTP_REFERER']) ? $this->aryServerInfo['HTTP_REFERER'] : '';
     }

     /**
    * getScriptUrl
    *
    * SCRIPT_URLを返却する
    *
    * @param mixed $filter
    * @access public
    * @return void
    */
     public function getScriptUrl()
     {
         return isset($this->aryServerInfo['SCRIPT_URL']) ? $this->aryServerInfo['SCRIPT_URL'] : '';
     }

    /**
     * getHost
     *
     * ホスト情報の取得
     *
     * @param mixed $filter
     * @access public
     * @return void
     */
    public function getHost()
    {
        return isset($this->aryServerInfo['HTTP_X_FORWARDED_HOST']) ? $this->aryServerInfo['HTTP_X_FORWARDED_HOST'] : (isset($this->aryServerInfo['HTTP_HOST']) ? $this->aryServerInfo['HTTP_HOST'] : '');
    }

    /**
     * getScriptName
     *
     * スクリプト名称の取得
     *
     *
     * @param mixed $filter
     * @access public
     * @return void
     */
    public function getScriptName()
    {
        return isset($this->aryServerInfo['SCRIPT_NAME']) ? $this->aryServerInfo['SCRIPT_NAME'] : (isset($this->aryServerInfo['ORIG_SCRIPT_NAME']) ? $this->aryServerInfo['ORIG_SCRIPT_NAME'] : '');
    }

    /**
     * getCurrentPathInfo
     *
     * 現在のパス情報
     *
     * @param mixed $filter
     * @access public
     * @return void
     */
    public function getCurrentPathInfo()
    {
        return (!empty($this->aryServerInfo['PATH_INFO'])) ? $this->aryServerInfo['PATH_INFO'] : false;
    }

    /**
     * getCurrentActionPath
     *
     * 現在のアクションパス、パス情報の第二・第三引数
     *
     * @access public
     * @return void
     */
    public function getCurrentActionPath()
    {
        return $this->actionPath;
    }

    /**
     * getQueryString
     *
     * クエリストリングの取得
     *
     * @param mixed $filter
     * @access public
     * @return void
     */
    public function getQueryString()
    {
        return (!empty($this->aryServerInfo['QUERY_STRING'])) ? $this->aryServerInfo['QUERY_STRING'] : false;
    }

    /**
     * getStrippedOriginalUrl
     *
     * StrippedOriginalUrlの取得
     *
     * @param mixed $filter
     * @access public
     * @return void
     */
    public function getStrippedOriginalUrl()
    {
        return (!empty($this->aryServerInfo['StrippedOriginalUrl'])) ? $this->aryServerInfo['StrippedOriginalUrl'] : false;
    }

    /**
     * getUri
     *
     * the uniform resource identifier の取得
     *
     * @param mixed $filter
     * @access public
     * @return void
     */
    public function getUri()
    {
        $requestURI = (!empty($this->aryServerInfo['REQUEST_URI'])) ? $this->aryServerInfo['REQUEST_URI'] : false;
        if(!$this->isAbsUri())
        {
            return $requestURI;
        }
        return $this->getUriPrefix().$this->aryServerInfo['REQUEST_URI'];
    }

    /**
     * getScriptUri　SCRIPT_URIを取得する
     *
     * @param mixed $filter
     * @access public
     * @return void
     */
    public function getScriptUri()
    {
        return (!empty($this->aryServerInfo['SCRIPT_URI'])) ? $this->aryServerInfo['SCRIPT_URI'] : false;
    }
    /**
     * isAbsUri
     *
     * 絶対URLかどうか
     *
     * @param mixed $filter
     * @access public
     * @return void
     */
    public function isAbsUri()
    {
      $requestURI = (!empty($this->aryServerInfo['REQUEST_URI'])) ? $this->aryServerInfo['REQUEST_URI'] : false;
      return preg_match('/^http/', $requestURI);
    }

    /**
     * getCookie
     *
     * @param mixed $name
     * @param mixed $defaultValue
     * @access public
     * @return void
     */
    public function getCookie($name, $defaultValue = null)
    {
        $cookie = htmlspecialchars($_COOKIE[$name]);
        return (!empty($cookie)) ? $cookie: false;
    }

    /**
     * getAll
     *
     * @access public
     * @return void
     */
    public function getAll()
    {
        return $this->aryParameter;
    }
}

?>
