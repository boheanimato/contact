<?php
/**
 * BasicFormatter
 *
 *
 * @author     ashibuya
 *
 */
class BasicFormatter
{

    /**
     * _getHtmlspecialchars htmlspecialcharsを全配列に対して行う
     * @param mixed $aryParams
     * @access public
     * @return mixed
     */
    public function _getHtmlspecialchars($aryParams)
    {
        if (is_object($aryParams)) {
            return $aryParams;
        }
        $result = array();
        if (is_array($aryParams)) {
            foreach ($aryParams as $key => $val) {
                $result[$key] = self::_getHtmlspecialchars($val);
            }
        } else {
            $result = htmlspecialchars($aryParams, ENT_QUOTES, 'UTF-8');
        }
        return $result;
    }

    /**
     * _getHtmlspecialchars_decode htmlspecialchars_decodeを全配列に対して行う
     * @param mixed $aryParams
     * @access public
     * @return mixed
     */
    public function _getHtmlspecialchars_decode($aryParams)
    {
        if (is_object($aryParams)) {
            return $aryParams;
        }
        $result = array();
        if (is_array($aryParams)) {
            foreach ($aryParams as $key => $val) {
                $result[$key] = self::_getHtmlspecialchars_decode($val);
            }
        } else {
            $result = htmlspecialchars_decode($aryParams, ENT_QUOTES, 'UTF-8');
        }
        return $result;
    }


    /**
     * _getStrip_tags 文字列から HTML および PHP タグを取り除く
     * @param mixed $aryParams
     * @access public
     * @return mixed
     */
    public function _getStrip_tags($aryParams)
    {
        if(is_object($aryParams)){
            return $aryParams;
        }
        $result = array();
        if(is_array($aryParams)){
            foreach($aryParams as $key => $val)
            {
                $result[$key] = self::_getStrip_tags($val);
            }
        }else{
            $result = strip_tags($aryParams, ENT_QUOTES);
        }

        return $result;
    }

}

?>